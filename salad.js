
/*Das Javascript für die Salat-Seite*/

let saladList = [
    {
        "name": "Green salad with tomatoe",
        "prize": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        "imageUrl": "https://farm6.staticflickr.com/5087/5358599242_7251dc7de4.jpg",
        "dressing": [
            "French Dressing",
            "Italian Dressing"
        ]
    },
    {
        "name": "Tomato salad with mozzarella",
        "prize": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella"
        ],
        "imageUrl": "https://farm4.staticflickr.com/3130/5862973974_c107ed81ea.jpg",
        "dressing": [
            "French Dressing",
            "Italian Dressing"
        ]
    },
    {
        "name": "Field salad with egg",
        "prize": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            "Egg"
        ],
        "imageUrl": "https://farm9.staticflickr.com/8223/8372222471_662acd24f6.jpg",
        "dressing": [
            "French Dressing",
            "Italian Dressing"
        ]
    },
    {
        "name": "Rocket with parmesan",
        "prize": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            "Parmesan"
        ],
        "imageUrl": "https://farm8.staticflickr.com/7017/6818343859_bb69394ff2.jpg",
        "dressing": [
            "French Dressing",
            "Italian Dressing"
        ]
    }
]
saladList.forEach(salad => {
    var saladContent = document.querySelector("#saladContent");
    var saladDiv = document.createElement("div");
    saladDiv.innerHTML = '<div class="diffSalad"><img class = "saladImage" src="' + salad.imageUrl + '"><h3 class="header3">' + salad.name + '</h3><p class="paragraph2">' + salad.ingredients + '</p><div class="diffSaladHelp"><div class="flexBoxHelp"><select class="dressings"><option >' + salad.dressing[0] + '</option><option >' + salad.dressing[1] + '</option></select></div><div class="flexBoxHelp2"><div class="flexBoxHelp"><h3>' + salad.prize + '</h3></div><div class="flexBoxHelp"><button><img src="Bilder/pizza/shoppingcart.png" width="20px" onclick="shopping()"></button></div></div></div></div>';
    saladContent.appendChild(saladDiv);

});