var counterArea = document.querySelector("#counter");

var counter = 5;
function incrementValue(){
    counter++;
    counterArea.textContent = counter;
}
function decreaseValue(){
    counter--;
    counterArea.textContent = counter;
    if (counter == 0){
        counterArea.textContent = " Achtung, weniger als 0 geht nicht... Aktueller Counter: 0"
    }
}