/*Javascript für die Softdrinks*/
let softList = [
    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "https://farm1.staticflickr.com/71/203324363_b448827eb0.jpg",
        "volume": [
            "50cl",
            "100cl",
            "330cl"
        ]
    },
    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "https://farm1.staticflickr.com/684/32876893826_130576f75a.jpg",
        "volume": [
            "50cl",
            "100cl",
            "330cl"
        ]
    },
    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "https://farm4.staticflickr.com/3344/3593103557_bf47c0a3a2.jpg",
        "volume": [
            "50cl",
            "100cl",
            "330cl"
        ]
    },
    {
        "name": "Red bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "https://farm3.staticflickr.com/2391/2507916617_254348d40c.jpg",
        "volume": [
            "50cl",
            "100cl",
            "330cl"
        ]
    }
]
softList.forEach(soft => {
    var softDrinks = document.querySelector("#softDrinksContent");
    var softdiv = document.createElement("div");
    softdiv.innerHTML = '<div class="diffDrinks"><img class = "softDrinkImage" src="' + soft.imageUrl + '"><h3 class="header3">' + soft.name + '</h3><div class="diffSoftHelp"><div class="flexBoxHelp"><select class="dressings"><option>' + soft.volume[0] + '</option><option>' + soft.volume[1] + '</option><option>' + soft.volume[2] + '</select></div><div class="flexBoxHelp2"><div class="flexBoxHelp"><h3>' + soft.prize + '</h3></div><div class="flexBoxHelp"><button><img src="Bilder/pizza/shoppingcart.png" width="20px" onclick="shopping()"></button></div></div></div></div>';
    softDrinks.appendChild(softdiv);
});